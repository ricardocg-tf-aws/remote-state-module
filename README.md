# Remote State Backend

- Creates an S3 bucket for remote state use
- version enabled and server side encryption

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| Name | Name given for the S3 bucket | - | - |:yes:|

## Outputs

| Name | Description |
|------|-------------|
| bucket_name | Name for the deployed bucket|
| bucket_arn | Arn of the bucket |