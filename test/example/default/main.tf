module "simple_bucket"{
    source                              = "../../../"
    namespace                           = "test1"
    stage                               = "test1"
    name                                = "ricardocg"
    region                              = "us-east-1"
    force_destroy                       = true
}