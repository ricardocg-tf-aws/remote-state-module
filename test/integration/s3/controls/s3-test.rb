actual_s3_id = attribute('bucket_name')

control "bucket_test" do
    impact 1

    describe aws_s3_bucket(actual_s3_id) do
        it { should exist }
        it { should_not be_public }
    end
end