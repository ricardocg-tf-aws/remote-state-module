actual_s3_id = attribute('bucket_name')

control 's3' do
  impact 1

  describe_bucket = "aws s3 ls s3://#{actual_s3_id}"
  describe command (describe_bucket) do
    its('exit_status'){ should eq 0 }
  end
end